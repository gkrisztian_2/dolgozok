﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DolgozokFeladat
{
    #region Általános eljárások
    interface ITevekenyseg
    {
        void Dolgozik(int h);
        int Fizkiir();
        void Reset();
        string Summary();
    }
    #endregion
    #region Adatszerkezet
    class Dolgozo : ITevekenyseg
    {
        string nev, munkakor;
        int oraber, munkaora, szulEv;

        public Dolgozo(Tuple<string, int, string, int, int> t)
        {
            this.nev = t.Item1;
            this.munkakor = t.Item3;
            this.szulEv = t.Item2;
            this.oraber = t.Item4;
            this.munkaora = t.Item5;
        }
         
        public string Nev
        {
            get { return this.nev; }
            set { this.nev = value; }
        }
        public string Munkakor
        {
            get { return this.munkakor; }
            set { this.munkakor = value; }
        }
        public int Szulev
        {
            get { return this.szulEv; }
            set { this.szulEv = value; }
        }

        public int Oraber
        {
            get { return this.oraber; }
            set { this.oraber = value; }
        }

        public int Munkaora
        {
            get { return this.munkaora; }
            set { this.munkaora = value; }
        }

        public void Dolgozik(int ora)
        {
            this.munkaora += ora;
        }
        public int Fizkiir()
        {
            return this.munkaora * this.oraber;
        }

        public int Eletkor
        {
            get { return DateTime.Now.Year - this.szulEv; }
        }

        public void Reset()
        {
            this.munkaora = 0;
        }

        public string Summary() // plusz függvény
        {
            return string.Format($"Név: {Nev} ({Eletkor} éves, {Munkakor} - {Oraber} Ft/óra) eddig {Munkaora} órát dolgozott.");
        }


    }
    #endregion

    class Program
    {
        static List<Dolgozo> l = new List<Dolgozo>();

        #region Dolgozók létrehozása
        /// <summary>
        /// A dolgozók generálása randomszerűen történik. Meghatározott nevek, munkák közül választ, és rendel hozzá munkaórát és órabért.
        /// </summary>
        /// <param name="x"></param>
        static void GenerateWorkers(int x)
        {
            
            string[] names = { "Nagy Ferenc", "Kiss János", "Szarka Márió", "Mester Loránd", "Sós Dalma"};
            string[] jobs = { "festő", "autószerelő", "ács", "könyvelő", "ügyvéd", "rendőr" };
            const int MAX_ORA = 160; // 8 órában, 5 nap 1 héten
            Random rnd = new Random();

            for (int i = 0; i < x; i++)
            {
                l.Add(new Dolgozo(new Tuple<string, int, string, int, int>(names[rnd.Next(names.Length)], rnd.Next(1970, 2002), jobs[rnd.Next(jobs.Length)], rnd.Next(800, 2500), rnd.Next(MAX_ORA+1))));
            }

        }

        
        #endregion
        /// <summary>
        /// A programot bővítettem Tuple-lel, Interface-szel, naplózással, tárolással.
        /// Ezenkívül "szimulációként" a program futtatás elején random dolgozókat generál.
        /// Mivel random, és nincsenek pontos feltételek megadva, előfordulhatnak szürreális mezők.
        /// </summary>
        static void Main(string[] args)
        {
            l.Add(new Dolgozo(new Tuple<string, int, string, int, int>("Nagy Ferenc", 1982, "festő", 2500, 0)));
            Dolgozo feri = l.Find(x => x.Nev == "Nagy Ferenc" && x.Szulev == 1982);

            GenerateWorkers(4); // 4 = 4 új dolgozó
            feri.Dolgozik(10);
            Console.WriteLine(feri.Munkaora);
            feri.Reset();
            Console.WriteLine(feri.Munkaora);

            foreach (var item in l)
            {
                Console.WriteLine(item.Summary());
            }
            Dolgozo legtobbetDolgozott = l.Find(x => x.Fizkiir() == l.Max(y => y.Fizkiir()));
            Console.WriteLine($"A legtöbb órát {legtobbetDolgozott.Nev} ({legtobbetDolgozott.Munkaora} óra) teljesítette, fizetése: {legtobbetDolgozott.Fizkiir()} Ft");

            Honapvege();
            //System.Diagnostics.Process.Start("statisztika.txt");
            Console.ReadKey();
        }

        /// <summary>
        /// Hónap végén egy fájlba kerül mentésre a havi teljesítmény, majd resetelődik.
        /// A mentés olyan formátumban készül, hogy beolvasásra is alkalmas.
        /// Fájl: statisztika.txt
        /// A '[' karakteres kezdés egy új hónapot jelent.
        /// </summary>
        static void Honapvege()
        {  
            try
            {
                using(System.IO.StreamWriter sw = new System.IO.StreamWriter(@"statisztika.txt", true))
                {
                    sw.WriteLine($"[{DateTime.Now.ToShortDateString()}] Statisztika");
                    foreach (var item in l)
                    {
                        sw.WriteLine($"{item.Nev};{item.Szulev};{item.Munkakor};{item.Munkaora};{item.Oraber}");
                        item.Reset();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Nem sikerült a fájlba írás.\n{0}", ex.Message);
            }
        }


    }
}
